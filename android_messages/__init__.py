__version__ = "0.1.0"

import os
import signal
import subprocess
import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium_stealth import stealth
import qrcode

qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_M,
    box_size=10,
    border=4,
)

chrome_driver_path = "chromedriver"

options = webdriver.ChromeOptions()
options.add_argument("--window-size=1920,1080")
options.add_argument("--headless")

options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option("useAutomationExtension", False)
driverService = Service(chrome_driver_path)
driver = webdriver.Chrome(options=options, service=driverService)

stealth(
    driver,
    languages=["en-US", "en"],
    vendor="Google Inc.",
    platform="Win32",
    webgl_vendor="Intel Inc.",
    renderer="Intel Iris OpenGL Engine",
    fix_hairline=True,
)

url = "https://messages.google.com/web/authentication?pli=1"
driver.get(url)
element = WebDriverWait(driver, 120).until(
    EC.presence_of_element_located(
        (
            By.XPATH,
            "/html/body/mw-app/mw-bootstrap/div/main/mw-authentication-container/div/div/div/div[1]/div[2]/mw-qr-code",
        )
    )
)
# driver.find_element(By.CLASS_NAME, "mat-slide-toggle-bar").click()
out = driver.find_element(
    By.XPATH,
    "/html/body/mw-app/mw-bootstrap/div/main/mw-authentication-container/div/div/div/div[1]/div[2]/mw-qr-code",
).get_attribute("outerHTML")

off = 'data-qr-code="'
fff = out[out.index(off) + len(off) : out.index('"><canvas width=')]

qr.add_data(fff)
qr.make(fit=True)

img = qr.make_image(fill_color="black", back_color="white")
img.save("some_file.png")
# print(driver.find_element(By.CLASS_NAME, "qr-code ng-trigger ng-trigger-fadeIn ng-tns-c73-0 ng-star-inserted").text)

try:
    devnull = open("/dev/null", "w")
    p = subprocess.Popen(
        ["gwenview", "some_file.png"], stdout=devnull, stderr=devnull, shell=False
    )
    pid = p.pid
except:
    print("genview did not launch")


element = WebDriverWait(driver, 120).until(
    EC.presence_of_element_located(
        (
            By.XPATH,
            "/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list",
        )
    )
)

try:
    os.kill(pid, signal.SIGINT)
except:
    print("gwenview did not close")

time.sleep(5)

driver.find_element(
    By.XPATH,
    "/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item[1]/a",
).click()
time.sleep(2)
driver.find_element(
    By.XPATH,
    "/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item[1]/a",
).click()
time.sleep(1)

k = 0
k_prev = 0

time_n = int(time.time())
time_prev = int(time.time())

while True:
    k_prev = k
    k = len(
        driver.find_elements(
            By.XPATH,
            "/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item",
        )
    )

    if k_prev != k:
        time_prev = time_n

    time_n = int(time.time())
    driver.find_element(
        By.XPATH,
        f"/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item[{k-5}]/a",
    ).click()
    driver.find_element(
        By.XPATH,
        f"/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item[{k-5}]/a",
    ).send_keys(Keys.DOWN)
    time.sleep(0.1)
    if k == k_prev and time_n - time_prev > 10:
        print(k, k_prev, time_n, time_prev)
        break


outz = driver.find_elements(
    By.XPATH,
    "/html/body/mw-app/mw-bootstrap/div/main/mw-main-container/div/mw-main-nav/mws-conversations-list/nav/div[1]/mws-conversation-list-item",
)

for i in outz:
    print(i.find_element(By.XPATH, ".//a/div[2]/h3/span").text)

time.sleep(1)
driver.save_screenshot("screenshot.png")
